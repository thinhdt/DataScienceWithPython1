def sum(x, y):
    sum = x + y
    if sum in range(15, 20):
        return 20
    else:
        return sum
#define the function sum (x+y)
#if the sum belongs to 15-20 -> sum = 20 | otherwise return sum

print(sum(10, 6)) # 20, because in range 15-20
print(sum(10, 2)) #12 , out of range 15-20, so sum=12
print(sum(10, 12)) #22, out of range 12-20, so sum=22
